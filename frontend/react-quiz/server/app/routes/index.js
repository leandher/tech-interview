var api = require('../api');

module.exports  = function(app) {
    
    app.route('/api/quiz')
        .post(api.save)
        .get(api.getAll);

    app.route('/api/quiz/:quizId')
        .delete(api.remove)
        .get(api.findOne)
        .put(api.update);
};