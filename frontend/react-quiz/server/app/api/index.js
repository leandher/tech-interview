var db = require('../../config/database');

var api = {}

api.save = function (req, res) {
    var quiz = req.body;
    delete quiz._id;
    db.insert(quiz, function (err, newDoc) {
        if (err) {
            return res.status(500).end();
        }
        res.json(newDoc._id);
    });
};

api.findOne = function (req, res) {
    db.findOne({ _id: req.params.quizId }, function (err, doc) {
        if (err) {
            return res.status(500).end();
        }
        res.json(doc);
    });
};

api.update = function (req, res) {
    db.update({ _id: req.params.quizId }, req.body, function (err, numReplaced) {
        if (err) return console.log(err);
        if (numReplaced) {
            res.status(200).end();
        } else {
            res.status(500).end();
        }
    });
};

api.getAll = function (req, res) {
    db.find({}).exec(function (err, doc) {
        if (err) {
            return res.status(500).end();
        }
        res.json(doc);
    });
};

api.remove = function (req, res) {

    db.remove({ _id: req.params.quizId }, {}, function (err, numRemoved) {
        if (err) return console.log(err);
        if (numRemoved) {
            res.status(200).end();
        } else {
            res.status(500).end();
        }
    });
};


module.exports = api;