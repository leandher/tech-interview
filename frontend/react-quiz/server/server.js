var http = require('http')
    , app = require('./config/express')
db = require('./config/database');

http.createServer(app).listen(8000, function () {
    console.log('Listening on port: ' + this.address().port);
});

var quiz =
{
    "title": "GOT",
    "questions": [
        {
            "question": "What is your spirit animal ?",
            "answers": [
                {
                    "option": "A",
                    "content": "Wolf"
                },
                {
                    "option": "B",
                    "content": "Lion"
                },
                {
                    "option": "C",
                    "content": "Dragon"
                }
            ],
            "answer": "A"
        },
        {
            "question": "What do you value most ? ",
            "answers": [
                {
                    "option": "A",
                    "content": "Loyalty"
                },
                {
                    "option": "B",
                    "content": "Money"
                },
                {
                    "option": "C",
                    "content": "Power"
                }
            ],
            "answer": "B"
        },
        {
            "question": "Who's your favorite family member ?",
            "answers": [
                {
                    "option": "A",
                    "content": "My Parent"
                },
                {
                    "option": "B",
                    "content": "My Sibling"
                },
                {
                    "option": "C",
                    "content": "My Grandparent"
                }
            ],
            "answer": "B"
        },
        {
            "question": "Choose an element",
            "answers": [
                {
                    "option": "A",
                    "content": "Water"
                },
                {
                    "option": "B",
                    "content": "Air"
                },
                {
                    "option": "C",
                    "content": "Fire"
                }
            ],
            "answer": "A"
        },
        {
            "question": " Which Castle would you live in ? ",
            "answers": [
                {
                    "option": "A",
                    "content": "Winterfell"
                },
                {
                    "option": "B",
                    "content": "Casterly Rock"
                },
                {
                    "option": "C",
                    "content": "DragonStone"
                }
            ],
            "answer": "C"
        }
    ]
}