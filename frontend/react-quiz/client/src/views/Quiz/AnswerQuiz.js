import React, { Component } from 'react';
import Quiz from '../../components/Quiz';
import { Panel } from 'react-bootstrap';

class AnswerQuiz extends Component {

    constructor() {
        super();
        this.state = { quiz: {} };
        this.onAnswerSelected = this.onAnswerSelected.bind(this)
    }

    componentDidMount() {
        const idQuiz = this.props.match.params.idQuiz;

        fetch('/api/quiz/' + idQuiz)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Error!")
            })
            .then(data => this.setState({ quiz: data }))
            .catch(error => this.setState({ message: error.message }));
    }

    renderQuiz(quiz) {
        if (quiz.questions) {
            return (
                <Quiz
                    onAnswerSelected={this.onAnswerSelected}
                    questions={quiz.questions}
                />
            );
        } else {
            return ('');
        }
    }

    /* This function is called when an answer is selected */
    onAnswerSelected(event) {
        const option = event.target.value;
        /* id = questionId + option */
        const questionId = event.target.id.split(option)[0];

        const quiz = this.state.quiz;
        const question = quiz.questions.find((quest) => quest.number === Number(questionId));
        /* Setting answered to true to disable the entire question */
        question.answered = true;

        /* Answer selected by user */
        const selectedAnswer = question.answers.find(answer => answer.option === option);
        /* Right answer */
        const rightAnswer = question.answers.find(answer => answer.option === question.answer);

        /* Setting style to show which is right or wrong */
        rightAnswer.style = 'rightAnswer';
        
        if (rightAnswer.option !== selectedAnswer.option) {
            selectedAnswer.style = 'wrongAnswer';
        }

        this.setState({ quiz: quiz });
    }

    render() {
        const { quiz } = this.state;
        return (
            <Panel bsStyle="default">
                <Panel.Heading>
                    <Panel.Title componentClass="h3">Quiz: {quiz.title}</Panel.Title>
                </Panel.Heading>
                <Panel.Body>
                    {this.renderQuiz(quiz)}
                </Panel.Body>
            </Panel>
        );
    }
}

export default AnswerQuiz;
