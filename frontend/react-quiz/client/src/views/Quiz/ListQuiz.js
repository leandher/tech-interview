import React, { Component } from 'react';
import { Panel, Modal, Button } from 'react-bootstrap';
import TableQuiz from '../../components/TableQuiz';

class ListQuiz extends Component {
    constructor() {
        super();
        this.state = { quizzes: [], message: '', show: false };
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.goToQuiz = this.goToQuiz.bind(this);
        this.goToEdit = this.goToEdit.bind(this);
        this.remove = this.remove.bind(this);
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow(idQuiz) {
        this.setState({ show: true, idQuiz: idQuiz });
    }

    componentDidMount() {
        this.load();
    }

    load() {
        fetch('/api/quiz')
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Error!")
            })
            .then(data => this.setState({ ...this.state, quizzes: data }))
            .catch(error => this.setState({ message: error.message }));
    }

    goToQuiz(idQuiz) {
        this.props.history.push("/answer/" + idQuiz);
    }

    remove() {
        fetch('/api/quiz/' + this.state.idQuiz, {
            method: 'delete'
        }).then(response => {
            if (response.ok) {
                this.handleClose();
                this.load();
            }
        }).catch(error => this.setState({ message: error.message }));
    }

    goToEdit(idQuiz) {
        this.props.history.push("/edit/" + idQuiz);
    }

    renderModal() {
        if (this.state.show) {
            return (
                <Modal.Dialog show={this.state.show.toString()}>
                    <Modal.Header>
                        <Modal.Title>Confirm action {this.state.show}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>Do you want to remove this quiz?</Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Close</Button>
                        <Button bsStyle="success" onClick={this.remove}>Confirm</Button>
                    </Modal.Footer>
                </Modal.Dialog>
            );
        }

        return ('');
    }

    render() {
        return (
            <div className="modal-container">
                <Panel bsStyle="default">
                    <Panel.Heading>
                        <Panel.Title componentClass="h3">Quiz List</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body>
                        <TableQuiz
                            quizzes={this.state.quizzes}
                            answerCallBack={this.goToQuiz}
                            removeCallBack={this.handleShow}
                            editCallBack={this.goToEdit} />
                    </Panel.Body>
                </Panel>
                {
                    this.renderModal()
                }

            </div>
        );
    }
}

export default ListQuiz;
