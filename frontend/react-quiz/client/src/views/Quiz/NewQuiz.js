import React, { Component } from 'react';
import FormQuiz from '../../components/Forms/FormQuiz';
import { Panel } from 'react-bootstrap';

class NewQuiz extends Component {
    constructor() {
        super();
        this.state = { title: {}, questions: [] };
        this.setTitle = this.setTitle.bind(this);
        this.save = this.save.bind(this);
        this.addQuestion = this.addQuestion.bind(this);
    }

    componentWillMount() {
        const idQuiz = this.props.match.params.idQuiz;

        fetch('/api/quiz/' + idQuiz)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Error!")
            })
            .then(data => this.setState({ ...data }))
            .catch(error => this.setState({ message: error.message }));
    }

    setTitle(event) {
        this.setState({ ...this.state, title: event.target.value });
    }

    save(event) {
        event.preventDefault();
        const requestInfo = {
            method: 'POST',
            body: JSON.stringify(this.state),
            headers: new Headers({
                'Content-type': 'application/json'
            })
        }

        fetch('/api/quiz', requestInfo)
            .then(response => {
                if (response.ok)
                    return response.json();

                throw new Error("Não foi possível efetuar login")
            })
            .then(data => {
                this.props.history.push('/list');
            })
            .catch(error => alert(error.message));
    }

    addQuestion(question) {
        const number = this.state.questions.length + 1;
        question.number = number;
        this.state.questions.push(question);

        this.setState(this.state);
    }

    render() {
        return (
            <Panel bsStyle="default">
                <Panel.Heading>
                    <Panel.Title componentClass="h3">Quiz</Panel.Title>
                </Panel.Heading>
                <Panel.Body>
                    <FormQuiz
                        setTitle={this.setTitle}
                        onSubmit={this.save}
                        addQuestion={this.addQuestion}
                        canSave={this.state.questions.length > 0}
                        questions={this.state.questions} />
                </Panel.Body>
            </Panel>
        );
    }
}

export default NewQuiz;
