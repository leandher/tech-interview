import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Home from './views/Home/Home';
import NewQuiz from './views/Quiz/NewQuiz';
import ListQuiz from './views/Quiz/ListQuiz';
import AnswerQuiz from './views/Quiz/AnswerQuiz';

ReactDOM.render((
    <Router>
        <App>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/new" component={NewQuiz} />
                <Route path="/edit/:idQuiz" component={NewQuiz} />
                <Route path="/list" component={ListQuiz} />
                <Route path="/answer/:idQuiz" component={AnswerQuiz} />
            </Switch>
        </App>
    </Router>
), document.getElementById('root'));
registerServiceWorker();
