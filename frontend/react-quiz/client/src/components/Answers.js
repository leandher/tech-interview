import React, { Component } from 'react';


class Answers extends Component {
    constructor() {
        super();
        this.state = { disabled: false };
    }

    render() {
        const { answer, answered, questionId, onAnswerSelected } = this.props;
        return (
            <li className={"answers "+answer.style}>
                <input
                    type="radio"
                    className="radioButton"
                    name={questionId}
                    id={questionId+answer.option}
                    value={answer.option}
                    disabled={answered}
                     onChange={onAnswerSelected}
                />

                <label className="radioLabel" htmlFor={questionId+answer.option}>
                    {answer.content}
                </label>

            </li>
        );
    }
}

export default Answers;