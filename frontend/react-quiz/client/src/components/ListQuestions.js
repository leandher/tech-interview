import React, { Component } from "react";
import Question from '../components/Question';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

class ListQuestions extends Component {

    render() {
        const { question } = this.props;
        return (
            <ListGroup>
                <Question
                    number={question.number}
                    content={question.title}
                />
                {
                    question.answers.map(answer => {
                        let style = 'danger';
                        if (answer.option === question.answer) {
                            style = 'success'
                        }
                        return (
                            <ListGroupItem key={answer.option} bsStyle={style}>
                                {answer.option + '. ' + answer.content}
                            </ListGroupItem>
                        );
                    })
                }
            </ListGroup>
        );
    }
}

export default ListQuestions;