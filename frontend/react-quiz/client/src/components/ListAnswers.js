import React, { Component } from "react";
import { ListGroup, ListGroupItem } from 'react-bootstrap';

class ListAnswers extends Component {

    render() {
        const { answers } = this.props;
        return (
            <ListGroup>
                {
                    answers.map(answer => {
                        return (
                            <ListGroupItem key={answer.option}>
                                {answer.option + '. ' + answer.content}
                            </ListGroupItem>
                        );
                    })
                }
            </ListGroup>
        );
    }
}

export default ListAnswers;