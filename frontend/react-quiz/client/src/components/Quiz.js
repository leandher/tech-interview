import React, { Component } from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import Question from '../components/Question';
import Answers from '../components/Answers';

class Quiz extends Component {

    renderAnswers(answer, question) {
        return (
            <Answers
                key={answer.option}
                answer={answer}
                answered={question.answered}
                questionId={question.number}
                onAnswerSelected={this.props.onAnswerSelected}
            />

        );
    }

    render() {
        return (
            <ListGroup>
                {
                    this.props.questions.map(question => {
                        return (
                            <ListGroupItem key={question.number}>
                                <Question 
                                number={question.number}
                                content={question.title} />
                                <ul className="answerOptions">
                                    {question.answers.map((answer) => this.renderAnswers(answer, question))}
                                </ul>
                            </ListGroupItem>
                        );
                    })
                }
            </ListGroup>
        );
    }
}

export default Quiz;