import React, { Component } from 'react';

class Question extends Component {
    render() {
        const { number, content } = this.props;
        return (
            <h2 className="question">{number + '. ' + content}</h2>
        );
    }
}

export default Question;