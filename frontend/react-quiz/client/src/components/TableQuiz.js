import React, { Component } from 'react';
import { Table, Button, ButtonToolbar, Glyphicon } from 'react-bootstrap';

class TableQuiz extends Component {
    render() {
        const { quizzes, answerCallBack, removeCallBack, editCallBack } = this.props;
        return (
            <Table responsive>
                <thead>
                    <tr>
                        <th >Title</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        quizzes.map((quiz) => {
                            return (
                                <tr key={quiz._id}>
                                    <td colSpan="6">{quiz.title}</td>
                                    <td className="pull-right">
                                        <ButtonToolbar>
                                            <Button bsStyle="primary" onClick={() => answerCallBack(quiz._id)}>
                                                <Glyphicon glyph="question-sign" />
                                            </Button>
                                            <Button bsStyle="success" onClick={() => editCallBack(quiz._id)}>
                                                <Glyphicon glyph="edit" />
                                            </Button>
                                            <Button bsStyle="danger" onClick={() => removeCallBack(quiz._id)}>
                                                <Glyphicon glyph="remove-sign" />
                                            </Button>
                                        </ButtonToolbar>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </Table>
        );
    }
}

export default TableQuiz;