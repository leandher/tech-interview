import React, { Component } from "react";
import { Form, FormGroup, ControlLabel, FormControl, Button, ButtonToolbar } from 'react-bootstrap';
import FormQuestion from "./FormQuestion";
import ListQuestions from "../ListQuestions";

class FormQuiz extends Component {
    constructor() {
        super();
        this.state = { question: { title: {}, answers: [], answer: '' }, isShow: false };

        this.setTitle = this.setTitle.bind(this);
        this.setRightAnswer = this.setRightAnswer.bind(this);
        this.setIsShow = this.setIsShow.bind(this);
        this.save = this.save.bind(this);
        this.addAnswer = this.addAnswer.bind(this);
    }

    setTitle(event) {
        const question = this.state.question;
        question.title = event.target.value;
        this.setState({ ...this.state, question: question });
    }

    setRightAnswer(event) {
        const question = this.state.question;
        question.answer = event.target.value;
        this.setState({ ...this.state, question: question });
    }

    setIsShow() {
        this.setState({ ...this.state, isShow: true })
    }

    save() {
        this.props.addQuestion(this.state.question);
        this.setState({ question: { title: {}, answers: [], answer: '' }, isShow: false });
    }

    addAnswer(answer) {
        /* Get uppercase letters using ASCII code */
        const option = String.fromCharCode(65 + this.state.question.answers.length);
        answer.option = option;
        this.state.question.answers.push(answer);

        this.setState(this.state);
    }

    renderFormQuestion() {
        if (this.state.isShow) {
            return (
                <FormQuestion
                    setTitle={this.setTitle}
                    setRightAnswer={this.setRightAnswer}
                    onSubmit={this.save}
                    addAnswer={this.addAnswer}
                    canSave={this.state.question.answers.length > 3}
                    answers={this.state.question.answers}
                />
            );
        }

        return ('');
    }

    render() {
        const { setTitle, onSubmit, canSave, questions } = this.props;
        return (
            <Form onSubmit={onSubmit}>
                <FormGroup controlId="formTitle">
                    <ControlLabel>Title</ControlLabel>{' '}
                    <FormControl type="text" placeholder="Quiz Title" onChange={setTitle} />
                </FormGroup>{' '}
                {
                    questions.map(question => {
                        return (
                            <ListQuestions
                                key={question.number}
                                question={question}
                            />
                        );
                    })
                }
                <ButtonToolbar>
                    <Button bsStyle="success" disabled={this.state.isShow} onClick={this.setIsShow}>Add question</Button>
                    <Button type="submit" disabled={!canSave} bsStyle="success">Save Quiz</Button>
                </ButtonToolbar>
                {
                    this.renderFormQuestion()
                }
            </Form>
        );
    }
}

export default FormQuiz;