import React, { Component } from "react";
import { FormGroup, ControlLabel, FormControl, Button, Glyphicon, Col } from 'react-bootstrap';

class FormAnswer extends Component {

    render() {
        const { setContent, onSubmit } = this.props;
        return (
            <div className="marginTop2">
                <Col md={6}>
                    <FormGroup inline="true" controlId="formContent">
                        <ControlLabel>Content</ControlLabel>{' '}
                        <FormControl type="text" placeholder="Option Content" onChange={setContent} />
                    </FormGroup>{' '}
                </Col>
                <Col md={6} className="marginTop2">
                    <Button bsStyle="success" onClick={onSubmit}><Glyphicon glyph="plus"></Glyphicon></Button>
                </Col>
            </div>
        );
    }
}

export default FormAnswer;