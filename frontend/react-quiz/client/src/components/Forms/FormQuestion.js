import React, { Component } from "react";
import { FormGroup, ControlLabel, FormControl, Button, Glyphicon, Row, Col } from 'react-bootstrap';
import FormAnswer from "./FormAnswer";
import ListAnswers from "../ListAnswers";

class FormQuestion extends Component {

    constructor() {
        super();
        this.state = { answer: { content: '' }, isShow: false };
        this.setContent = this.setContent.bind(this);
        this.setIsShow = this.setIsShow.bind(this);
        this.save = this.save.bind(this);
    }

    setContent(event) {
        this.setState({ ...this.state, answer: { content: event.target.value } });
    }

    setIsShow() {
        this.setState({ ...this.state, isShow: true })
    }

    save() {
        this.props.addAnswer(this.state.answer);
        this.setState({ answer: { content: '' }, isShow: false });
    }

    renderFormAnswer() {
        if (this.state.isShow) {
            return (
                <FormAnswer
                    setContent={this.setContent}
                    onSubmit={this.save}
                />
            );
        }
        return ('');
    }

    render() {
        const { setTitle, setRightAnswer, onSubmit, canSave, answers } = this.props;
        return (
            <div className="marginTop2">
                <Row>
                    <Col md={6}>
                        <FormGroup inline="true" controlId="formTitle">
                            <ControlLabel>Title</ControlLabel>{' '}
                            <FormControl type="text" placeholder="Question Title" onChange={setTitle} />
                        </FormGroup>{' '}

                        <FormGroup controlId="formRightAnswer">
                            <ControlLabel>Right answer</ControlLabel>{' '}
                            <FormControl componentClass="select" placeholder="Select right answer" disabled={answers.length === 0} onChange={setRightAnswer}>
                                <option value="">Select right answer</option>
                                {
                                    answers.map(answer => {
                                        return (
                                            <option key={answer.option} value={answer.option}>{answer.content}</option>
                                        );
                                    })
                                }
                            </FormControl>
                        </FormGroup>{' '}
                    </Col>
                    <Col md={6} className="marginTop2">
                        <Button bsStyle="success" disabled={!canSave} onClick={onSubmit}><Glyphicon glyph="plus"></Glyphicon></Button>
                    </Col>
                </Row>
                <ListAnswers
                    answers={answers}
                />
                <Row>
                    <Col md={12}>
                        <Button bsStyle="success" disabled={this.state.isShow} onClick={this.setIsShow}>Add answer</Button>
                    </Col>
                </Row>
                <Row>
                    {
                        this.renderFormAnswer()
                    }
                </Row>
            </div>
        );
    }
}

export default FormQuestion;