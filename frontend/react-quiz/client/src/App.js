import React, { Component } from 'react';
import { Nav, NavItem, Navbar } from 'react-bootstrap';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Navbar light="true" color="blue-grey lighten-5" expand="lg">
          <Navbar.Header>
            <Navbar.Brand>
              <a href="/">
                React Quiz
              </a>
            </Navbar.Brand>
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <NavItem eventKey={1} href="/new">
                New Quiz
              </NavItem>
              <NavItem eventKey={2} href="/list">
                Answer a quiz
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <div className="container">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default App;
